<?php
Class Data extends CI_Controller{

	public function __construct(){
		parent::__construct();
		$this->load->model("model_data");
		$this->load->helper('url');
		
	}
	public function index()
		{
			$data['list'] = $this->model_data->tampil_data()->result();
			$data['total_data'] = $this->model_data->hitungData();
			//$data['kodeunik'] = $this->model_data->buat_kode(); 

			$this->load->view("data_list",$data);
		}

	public function add()
		{
			$this->load->model("model_data");
			$data['tipe'] = "Add";
			if(isset($_POST['tombol_submit'])){
			$this->model_data->simpan($_POST);
			redirect("data");
		}

		$this->load->view("form",$data);
	}

	public function edit($id)
		{
			$this->load->model("model_data");
			$data['tipe'] = "Edit";
			$data['default'] = $this->model_data->get_default($id);



			if(isset($_POST['tombol_submit'])){
			$this->model_data->update($_POST, $id);
			redirect("data");
		}

		$this->load->view("form",$data);
	}

	public function delete($id)	
		{
			$this->load->model("model_data");
			$this->model_data->hapus($id);
			redirect("data");
		}
	}