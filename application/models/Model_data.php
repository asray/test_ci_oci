<?php
defined('BASEPATH') OR exit('No direct script access allowed');

Class Model_data extends CI_Model{

	
	public function tampil_data(){
		return $this->db->get('DATA_PELAMAR');
	}
	public function hitungData ()
	{   
		$query = $this->db->get('DATA_PELAMAR');
		if($query->num_rows()>0)
		{
		  return $query->num_rows();
		}
		else
		{
		  return 0;
		}
	}
	public function simpan($post){
		$ID = $this->db->escape($post['ID']);
		$NAMA = $this->db->escape($post['NAMA']);
		$POSISI = $this->db->escape($post['POSISI']);

		$pesan='Data Berhasil Disimpan';

		$sql = $this->db->query("INSERT INTO DATA_PELAMAR VALUES ($ID, $NAMA, $POSISI)");
		if($sql)
			return true;
		return false;
	}
	public function get_default($id){
		$sql = $this->db->query("SELECT * FROM DATA_PELAMAR WHERE ID = ".intval($id));
		if($sql->num_rows() > 0)
			return $sql->row_array();
		return false;
	}

	public function update($post, $id){
		$ID = $this->db->escape($post['ID']);
		$NAMA = $this->db->escape($post['NAMA']);
		$POSISI = $this->db->escape($post['POSISI']);
		$sql = $this->db->query("UPDATE DATA_PELAMAR SET ID = $ID, NAMA = $NAMA, POSISI = $POSISI WHERE ID = ".intval($id));

		return true;
	}
	public function hapus($ID){
        $this->db->where('ID', $ID);
        $this->db->delete('DATA_PELAMAR');
	}
	public function buat_kode()   {

		  $this->db->select('RIGHT(DATA_PELAMAR.ID,4) as kode', FALSE);
		  $this->db->order_by('ID','DESC');    
		  $this->db->limit(1);    
		  $query = $this->db->get('DATA_PELAMAR');      //cek dulu apakah ada sudah ada kode di tabel.    
		  if($query->num_rows() <> 0){      
		   //jika kode ternyata sudah ada.      
		   $data = $query->row();      
		   $kode = intval($data->kode) + 1;    
		  }
		  else {      
		   //jika kode belum ada      
		   $kode = 1;    
		  }

		  $kodemax = str_pad($kode, 4, "0", STR_PAD_LEFT); // angka 4 menunjukkan jumlah digit angka 0
		  $kodejadi = "perI-9921-".$kodemax;    // hasilnya ODJ-9921-0001 dst.
		  return $kodejadi;  
	}
}