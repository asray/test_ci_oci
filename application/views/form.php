<!doctype html>
<html lang="en">
<head>
	<base href="<?=base_url()?>">
	<meta charset="UTF-8">
	<title>Create</title>
	<link rel="stylesheet" href="css/style.css">
	<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css" integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm" crossorigin="anonymous">
</head>
<body>
	<div class="container">
		<h1><?=$tipe?> Data</h1>
		
		<form method="post" class="form-horizontal">
			<div class="form-group">
			
				<div class="col-sm-10">
				<label>ID pelamar</label>				
					<input type="number" class="form-control" name="ID" value="<?=isset($default['ID'])? $default['ID'] : ""?>"placeholder="" required oninvalid="this.setCustomValidity('data tidak boleh kosong')"oninput="setCustomValidity('')"> 
				</div>
				<div class="col-sm-10">
				<label>Nama Pelamar</label>
					<input type="text" class="form-control" name="NAMA" value="<?=isset($default['NAMA'])? $default['NAMA'] : ""?>"placeholder="" required oninvalid="this.setCustomValidity('data tidak boleh kosong')"oninput="setCustomValidity('')"> 
				</div>
				<div class="col-sm-10">
				<label>Posisi</label>
					<input type="text" class="form-control" name="POSISI" value="<?=isset($default['POSISI'])? $default['POSISI'] : ""?>"placeholder="" required oninvalid="this.setCustomValidity('data tidak boleh kosong')"oninput="setCustomValidity('')"> 
				</div>				
				
			</div>
			
			
			<center>
				<button name="tombol_submit" class="btn btn-primary">
					Simpan
				</button>
				<a href="data" class="btn btn-warning">Batal</a>
			</center>


		</form>
	</div>
</body>
</html>