<!DOCTYPE html>
<html lang="en">
<head>
 <meta charset="utf-8">
 <title>Read</title>
 <link rel="stylesheet" href="<?php echo base_url('assets/css/style1.css')?>">

 <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" integrity="sha384-BVYiiSIFeK1dGmJRAkycuHAHRg32OmUcww7on3RYdg4Va+PmSTsz/K68vbdEjh4u" crossorigin="anonymous">
    <link href="https://fonts.googleapis.com/css?family=Roboto" rel="stylesheet">
    <style type="text/css">
        body {
            font-family: 'Roboto', sans-serif;
        }
    </style>
</head>
<body>
<div class="loader-wrapper">
			<div class="loader"></div>
		</div>
<div class="container">
	<h1>Data Table</h1> 	
	<p><a href="data/add" class="btn btn-primary">Tambah Data</a></p>
    <div class="table-responsive table-bordered">	
	<table class="table">
		<thead>
			<tr>
				<th>No</th>
				<th>ID Pelamar</th>
				<th>Nama</th>	
				<th>Posisi</th>				
				<th>aksi</th>
				<th></th>
			</tr>
		</thead>
		<tbody>
		<?php 
		$no = 1;
		foreach($list as $row)
	{ 
		?>
		<tr>
			<td><?php echo $no++ ?></td>
			<td><?php echo $row->ID ?></td>
			<td><?php echo $row->NAMA ?></td>
			<td><?php echo $row->POSISI ?></td>
			<td>
			 <?php echo anchor('data/edit/'.$row->ID, 'Edit', array('class'=>'btn btn-warning')); ?>
			 <?php echo anchor('data/delete/'.$row->ID, 'Hapus', array('class'=>'btn btn-danger', 'onclick'=>"return confirmDialog();")); ?>
			</td>
		</tr>
		<?php 
	} ?>
			<tr>
			<th>total : <?php echo $total_data; ?></th>            
			<th></th>
			</tr>
			
		</tbody>
	</table>
</div>
<script type="text/javascript" src="<?php echo base_url('assets/js/script.js') ?>"></script>
<script>
function confirmDialog() {
 return confirm('Apakah anda yakin akan menghapus data ini?')
}
</script>
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js" integrity="sha384-Tc5IQib027qvyjSMfHjOMaLkfuWVxZxUPnCJA7l2mCWNIpG9mGCD8wGNIcPD7Txa" crossorigin="anonymous"></script>
</body>
</html>
